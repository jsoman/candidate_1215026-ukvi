Candidate:1215026
---------------------------
Project directory structure
src
  + test
    + java                          
      + com.assessment                  
        + pages
        + tests
          + Runner       
     	  + Steps         
    + resources
      + features                                 
    + webdriver 
target
    +pom.xml
    +README.md
    +serenity.confi
    +serenity.properties
--------------------------------    
Technologies Used 
•	Java
•	BDD
•	Serenity
•	Cucumber
•	Junit
•	SerenityRest
•	Maven
•	Intellij IDE
--------------------------------
