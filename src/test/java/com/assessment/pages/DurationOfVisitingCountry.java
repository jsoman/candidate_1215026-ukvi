package com.assessment.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.webelements.RadioButtonGroup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DurationOfVisitingCountry extends PageObject {


    @Step("This step will allow the user to select dependant option")
    public void selectDependantOption(String option) {

        List<WebElement> radioButtons = getDriver().findElements(By.name("response"));
        RadioButtonGroup group = new RadioButtonGroup(radioButtons);
        group.selectByValue(option);
    }

    @Step("This allows the user select the duration of visit")
    public void selectDurationOfVisit(String duration) {
        List<WebElement> radioButtons = getDriver().findElements(By.name("response"));
        RadioButtonGroup group = new RadioButtonGroup(radioButtons);
        group.selectByValue(duration);
        waitForTextToAppear("How long are you planning to study in the UK for?");
    }

    @Step("This step will allow the user to select nextstep button")
    public void clickOnNextStepButton() {
        $(By.xpath("//*[@id=\"current-question\"]/button")).click();
    }

}
