package com.assessment.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

public class NationalityOfCountry extends PageObject {

    @Step("This step will verify the select nationality")
    public void selectTheNationality(String country) throws InterruptedException {
        WebElementFacade Cookies = $(By.xpath("//*[@id=\"global-cookie-message\"]/div[1]/div/div/div[2]/div[1]/button"));
        Cookies.click();

        WebElementFacade dropdownId = $(By.id("response"));
        selectFromDropdown(dropdownId, country);

        Thread.sleep(1000);
    }

    @Step("This step will allow the user to select the nationality")
    public void clickOnNextStepButton() {

        $(By.xpath("//*[@id=\"current-question\"]/button")).click();
    }

}
  