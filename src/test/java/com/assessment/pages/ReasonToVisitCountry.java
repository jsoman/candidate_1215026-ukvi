package com.assessment.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.webelements.RadioButtonGroup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ReasonToVisitCountry extends PageObject {

    @Step("This steps will allow the user to select the reason for visit")
    public void selectTheReasonForVisit(String reason) throws InterruptedException {
        List<WebElement> radioButtons = getDriver().findElements(By.name("response"));
        RadioButtonGroup group = new RadioButtonGroup(radioButtons);
        group.selectByValue(reason);
        Thread.sleep(1000);
    }

    @Step("This step will allow the user to click on nextstep button")
    public void clickOnNextStepButton() {

        $(By.xpath("//*[@id=\"current-question\"]/button")).click();

    }
}