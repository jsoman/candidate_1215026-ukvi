package com.assessment.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class VisaCriteriaOfCountry extends PageObject {

    public enum VisaCriteria {
        STUDY("You’ll need a visa to study in the UK"),
        TOURISM_NO_VISA("You won’t need a visa to come to the UK"),
        TOURISM_VISA("You’ll need a visa to come to the UK");

        private final String description;

        VisaCriteria(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }

    @Step("This step will verify criteria result as expected")
    public void verifyCriteriaResult(VisaCriteria visaCriteria) {
        if (visaCriteria.equals(VisaCriteriaOfCountry.VisaCriteria.STUDY)) {
            waitForTextToAppear(VisaCriteria.STUDY.getDescription());
            Assert.assertSame(visaCriteria.getDescription(), VisaCriteria.STUDY.getDescription());
        } else if (visaCriteria.equals(VisaCriteria.TOURISM_NO_VISA)) {
            waitForTextToAppear(VisaCriteria.TOURISM_NO_VISA.getDescription());
            Assert.assertSame(visaCriteria.getDescription(), VisaCriteria.TOURISM_NO_VISA.getDescription());
        } else if (visaCriteria.equals(VisaCriteria.TOURISM_VISA)) {
            waitForTextToAppear(VisaCriteria.TOURISM_VISA.getDescription());
            Assert.assertSame(visaCriteria.getDescription(), VisaCriteria.TOURISM_VISA.getDescription());
        }
    }

}
