package com.assessment.test;

import com.assessment.pages.DurationOfVisitingCountry;
import com.assessment.pages.NationalityOfCountry;
import com.assessment.pages.ReasonToVisitCountry;
import com.assessment.pages.VisaCriteriaOfCountry;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class VisaApplicationSteps {

    @Steps
    ReasonToVisitCountry reasonToVisitCountry;
    @Steps
    DurationOfVisitingCountry durationOfVisitingCountry;
    @Steps
    NationalityOfCountry nationalityOfCountry;
    @Steps
    VisaCriteriaOfCountry visaCriteriaOfCountry;

    @Given("^I provide a nationality of (.*)$")
    public void i_provide_a_nationality_of(String country) throws InterruptedException {

        nationalityOfCountry.open();
        nationalityOfCountry.selectTheNationality(country);
        nationalityOfCountry.clickOnNextStepButton();
    }

    @And("^I select the reason to (.*)$")
    public void i_select_the_reason_to(String reason) throws InterruptedException {
        reasonToVisitCountry.selectTheReasonForVisit(reason.toLowerCase());
        reasonToVisitCountry.clickOnNextStepButton();

    }


    @And("^I state that I am travelling (.*)$")
    public void i_state_that_i_am_travelling(String travelStatement) {
        if (!travelStatement.isEmpty() && travelStatement.contains("longer_than_six_months")) {
            durationOfVisitingCountry.selectDurationOfVisit(travelStatement);
        } else if (!travelStatement.isEmpty() && travelStatement.contains("No partner or family member")) {
            durationOfVisitingCountry.selectDependantOption("no");
        }
    }

    @When("I (.*) submit the form$")
    public void i_submit_the_form(String action) {
        if (action.trim().equalsIgnoreCase("do")) {
            durationOfVisitingCountry.clickOnNextStepButton();
        }
    }

    @Then("^I will be informed that (.*)$")
    public void i_will_be_informed_that(String visaCriteria) {
        if (visaCriteria.equalsIgnoreCase("Study")) {
            visaCriteriaOfCountry.verifyCriteriaResult(VisaCriteriaOfCountry.VisaCriteria.STUDY);
        } else if (visaCriteria.equalsIgnoreCase("Tourism_No_Visa")) {
            visaCriteriaOfCountry.verifyCriteriaResult(VisaCriteriaOfCountry.VisaCriteria.TOURISM_NO_VISA);
        } else if (visaCriteria.equalsIgnoreCase("Tourism_Visa"))
            visaCriteriaOfCountry.verifyCriteriaResult(VisaCriteriaOfCountry.VisaCriteria.TOURISM_VISA);
    }

}
