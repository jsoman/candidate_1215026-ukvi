Feature: Confirm whether a visa is required to visit the UK

  Scenario Outline: Confirm whether <Country> require a <Reason> visa to visit the UK
    Given I provide a nationality of <Country>
    And I select the reason to <Reason>
    And I state that I am travelling <TravelStatement>
    When I <FormAction> submit the form
    Then I will be informed that <VisaCriteria>
    Examples:
      | Country | Reason  | TravelStatement             | FormAction | VisaCriteria    |
      | Japan   | Study   | longer_than_six_months      | do         | Study           |
      | Japan   | Tourism |                             | do not     | Tourism_No_Visa |
      | Russia  | Tourism | No partner or family member | do         | Tourism_Visa    |
